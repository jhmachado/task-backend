<?php

namespace Task;

class RouteProvider
{
    /**
     * @var array
     */
    private $controllers;

    /**
     * @var string
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function callRouteAction(string $method, string $path)
    {
        $action = Route::resolveRoute($method, $path);
        $actionExploded = explode('@', $action);

        $controllerClass = $this->controllers[$actionExploded[0]];

        return $this->container->resolveMethod($controllerClass, $actionExploded[1]);
    }

    public function registerController(string $className, $path): void
    {
        $this->controllers[$className] = $path;
    }
}