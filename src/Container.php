<?php

namespace Task;

use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;

class Container
{
    /**
     * @var array
     */
    private $classesMap;

    public function set(string $class, array $defaultValues): void
    {
        if (isset($this->classesMap[$class])) {
            throw new \LogicException("The class {$class} is already mapped");
        }

        $this->classesMap[$class] = $defaultValues;
    }

    public function get(string $class): object
    {
        $reflector = new ReflectionClass($class);
        if ($reflector->isInstantiable()) {
            $instance = $this->getInstance($reflector);
            return $instance;
        }

        throw new \LogicException("The class {$class} is not instantiable");
    }

    public function resolveMethod(string $className, string $method)
    {
        $instance = $this->get($className);
        
        $reflectorMethod = new ReflectionMethod($className, $method);
        // $reflectorMethod->get

        $parameters = $reflectorMethod->getParameters();
        $dependencies = $this->getDependencies($parameters, $className);

        $result = $reflectorMethod->invokeArgs($instance, $dependencies);
        return $result;
    }

    private function getInstance(ReflectionClass $reflector): object
    {
        $constructor = $reflector->getConstructor();
        if (is_null($constructor)) {
            return $reflector->newInstance();
        }

        $parameters = $constructor->getParameters();
		$dependencies = $this->getDependencies($parameters, $reflector->getName());

		return $reflector->newInstanceArgs($dependencies);
    }

    private function getDependencies(array $parameters, string $class): array
    {
        $dependencies = [];
        foreach ($parameters as $parameter) {
            $dependency = $parameter->getClass();
            
            if (is_null($dependency)) {
                $defaultValue = $this->getDefaultOrCry($parameter, $class);
                $dependencies[] = $defaultValue;
                continue;
            }

            $dependencies[] = $this->get($dependency->name);
        }

        return $dependencies;
    }

    private function getDefaultOrCry(ReflectionParameter $parameter, string $class)
    {
        if (isset($this->classesMap[$class][$parameter->getName()])) {
            return $this->classesMap[$class][$parameter->getName()];
        }

        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }

        throw new \LogicException("Can not resolve class dependency: {$parameter}");
    }
}
