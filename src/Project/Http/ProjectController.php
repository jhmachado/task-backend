<?php

namespace Task\Project\Http;

use Task\Project\Service\ProjectService;

class ProjectController
{
    public function __construct(ProjectRequest $request)
    {
        // var_dump($request);
    }

    public function index(ProjectService $service): string
    {
        $service->test();
        return "It works";
    }
}