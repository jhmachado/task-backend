<?php

namespace Task;

class Route
{
    /**
     * @var array
     */
    private static $routes;

    public static function get(string $path, string $method): void
    {
        self::$routes['get'][$path] = $method;
    }

    public static function resolveRoute(string $method, string $route): string
    {
        return self::$routes[$method][$route] ?? '';
    }
}
