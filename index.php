<?php

declare(strict_types = 1);

require './vendor/autoload.php';

use Task\Container;
use Task\Project\Http\ProjectController;
use Task\Project\Http\ProjectRequest;
use Task\Project\Project;

// $projectController = new ProjectController();

$container = new Container();
$container->set(ProjectRequest::class, ['value' => 'test']);
$controller = $container->get(ProjectController::class);

// use Task\Project\Http\ProjectController;
use Task\Route;
use Task\RouteProvider;

Route::get('/projects', 'ProjectController@index');

$routeProvider = new RouteProvider($container);
$routeProvider->registerController('ProjectController', ProjectController::class);
$result = $routeProvider->callRouteAction('get', '/projects');

var_dump($result);
// $destination = Route::resolveRoute('get', '/projects');

$project = $container->get(Project::class);
var_dump($project);